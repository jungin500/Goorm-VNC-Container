# Ubuntu 14.04.5 LTS
Goorm.io Workspace [LXDE] Docker Container

    wget 'https://gitlab.com/jungin500/Goorm-VNC-Container/raw/master/install-lxde.sh' -O installer; chmod +x installer; ./installer


### 01. Configuration
---
Goorm.io 컨테이너 시작 설정

    # DBus System daemon
    mkdir -p /var/run/dbus
    rm /var/run/dbus/*
    dbus-daemon --system

    # SSH Daemon
    /usr/sbin/sshd-original -D

    # Init replacement; run-parts will recursively
    # run init scripts.
    run-parts /root/.local/bin &
    su -c "run-parts /home/jungin500/.local/bin" jungin500 &
    
### 02. Applications
---
설치되어 있는 어플리케이션

#### Base Applications
- `fcitx`, `fcitx-hangul`: 한글 키보드 입력용
- `tigervncserver` : VNC 서버 지원용 (기존 `tightvncserver`보다 지원성이 높음)
- `noVNC` : Websocketify를 이용한 웹 브라우저에서 접근할 수 있는 VNC 클라이언트(제공자 서버)
- `LXDE` : Lightweight X-Desktop Environment. 이만큼 가벼운 Desktop Environment도 없다.

#### Development Applications
- `VSCode` : Visual Studio Code는 각종 Code에 대한 Editing을 수행할 수 있다.
- `Eclipse Oxygen` : Java 개발용 IDE.
- `PyCharm Community Edition` : Python 개발용 IDE.

#### Works Application
- `Midori` : Firefox, Chrome 및 Chromium 등 Webkit 기반 대체 브라우져. `/dev/shm/`(공유 메모리)가 꽉 차 Crash되는 현상을 방지할 수 있는 Alternative 브라우져이다. 이 브라우져는 Shared memory를 사용하지 않는다.
- `Firefox` : 그럼에도 불구하고 지원하지 않는 사이트들에 접근하기 위한 브라우져.
